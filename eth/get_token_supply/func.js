const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

fdk.handle(function(input){
  const address = getAddressFromInput(input)

  if (!address){
    return {error: 'Please include an account'}
  }

  return api.stats.tokensupply(null, address).then((tokenSupplyResult)=>{
    if(tokenSupplyResult.status !== '1'){
     return {
       error : "Etherscan API failed"
     }
    }

    return {
      count : tokenSupplyResult.result
    }
  })
})

fdk.slack(function(result){
  let responseType = "in_channel"
  let blocks = []

  if (result.error) {
    responseType = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${result.error}`
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `Current number of tokens available: ${numberWithCommas(result.count)}`
      }
    })
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
})

const numberWithCommas = (x) => {
  const numSplit = x.toString().split('.')
  numSplit[0] =  numSplit[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")

  return numSplit.join('.')
}

// If input is passed in to slack as strings it is packaged and accessible via '_inputs: [var1, var2, ...]'
const getAddressFromInput = (input) => {
  let address

  if( input._inputs ){
    address = input._inputs[0]
  }

  if (input.address) {
    address = input.address
  }

  return address
}
