# get_token_supply

Gets the total count of an ERC-20 token

## Examples

### CLI

`a8 invoke crypto_eth.get_token_supply '{"address":"0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'`

## Input

address is the address of a given token

``` json
 {
   address: string
 }
```

## Output

count is number of tokens currently in existence

``` json
 {
   count: int
 }
```