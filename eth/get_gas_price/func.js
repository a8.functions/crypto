const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

const weiToGwei = 1000000000

fdk.handle(function(input){
  return api.proxy.eth_gasPrice().then((ethGasPriceResult)=>{

     const gasPriceWei = (parseInt(ethGasPriceResult.result,16))

     return {
      gwei : gasPriceWei/weiToGwei
     }
  })
})

fdk.slack(function(result){
  let responseType = "in_channel"
  let blocks = []

  if(!result.gwei){
    responseType = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: "Something went wrong"
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${result.gwei}  gwei`
      }
    })
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
})
