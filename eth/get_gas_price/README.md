# get_gas_price

Gets the price of gas for Ethereum transactions

## Examples

### CLI

`a8 invoke crypto_eth.get_gas_price`

### Input

`{}` or none

### Output

`gwei` current approximate price of gas in gwei  

``` json
 {
    gwei: int
 }
```
