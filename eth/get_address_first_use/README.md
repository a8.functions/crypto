# get_address_first_use

Gets the date of the first time an address made a transaction

## Examples

### cli

`a8 invoke crypto_eth.get_address_first_use '{"address":"0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'`

## Input

`address` is an eth address

``` json
{
    address : string
}
```

## Output

`unixTime` is the time in seconds since unix epoch.
`niceTime` is a well formated human readable string , eg `'2 Nov 2018 11:27:44`

``` json
 {
    unixTime: int,
    niceTime: DD Mon YYYY hh:mm:ss
 }
```
