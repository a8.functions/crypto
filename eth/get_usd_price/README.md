# get_usd_price

Gets price of an amount of eth in usd and btc

## Examples

### CLI

`a8 invoke crypto_eth.get_usd_price`

#### input

`wei` and `eth` add together, default `1` eth

``` json
{
    wei : int,
    ETH : int
}
```

#### Output

`usd` is the amount in usd of the eth

``` json
 {
    USD: int  
 }
```