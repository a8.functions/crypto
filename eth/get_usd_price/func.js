const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

const weiToEth = 1000000000000000000

fdk.handle(function(input){
  const totalEth = totalEthFromInput(input)

  return api.stats.ethprice().then((ethPriceResult)=>{
    if(ethPriceResult.status !== '1'){
      return {
        error : "Etherscan API failed"
      }
    }

    return {
      usd : ethPriceResult.result.ethusd*totalEth
    }
  })
})

fdk.slack(function(result){
  let blocks = []
  let response_type = "in_channel"
    
  if(!result.usd) {
    response_type = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: "Couldn't find eth for this account!" + result.error
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `$${result.usd}`
      }
    })
  }

  return {
    "response_type": response_type,
    "blocks": blocks
  }
})

fdk.sheets(function(result) {
  return {
    "sheets": [['ETH',`${result.usd}`]]
  }
})

// If input is passed in to slack as strings it is packaged and accessible via '_inputs: [var1, var2, ...]'
const totalEthFromInput = (input) => {
    let eth = 0
    let wei = 0

    if( input._inputs ){
      eth = input._inputs[0]
      if (input._inputs.length > 1){
        wei = input._inputs[1]
      }
    }

    if (input.wei){
      wei = input.wei
    }

    if (input.ETH) {
      eth = input.ETH
    }

    let totalEth = eth + wei/weiToEth

    if(totalEth === 0){
      totalEth = 1
    }

    return totalEth
}
