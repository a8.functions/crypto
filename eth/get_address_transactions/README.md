# get_address_transactions
gets a list of transactions an address has made 

#### example calls:
##### cli
`a8 invoke crypto_eth.get_address_transactions '{"address":"0xB8c77482e45F1F44dE1745F52C74426C631bDD52","offset":1}'`

## input
`address` The eth address  
`startblock` The block to start the search from, default `1` 
`endblock` The block to end the search on, default `latest`  
`offset` The number of transactions to display, default `1000`
`page` The `page` to get the transations from if there are more total transactions then the offset, default `1`  
`sort` The direction of sort, by blocknumber can be `asc` or `desc`, default `asc`


```
{
    address : string,
    startblock = int,
    endblock = string,
    page = int,
    offset = int,
    sort = string
}
```

## output
`transactions` is an array of transactions
```
{
    "transcations": [
      {
        "blockHash": string,
        "blockNumber": string,
        "confirmations": string,
        "contractAddress": string,
        "cumulativeGasUsed": string,
        "from": string,
        "gas": string,
        "gasPrice": string,
        "gasUsed": string,
        "hash": string,
        "input": string,
        "isError": string,
        "nonce": string,
        "timeStamp": string,
        "to": string,
        "transactionIndex": string,
        "txreceipt_status": string,
        "value": string
      }
    ]
}
```