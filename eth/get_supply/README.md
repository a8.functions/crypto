# get_supply

Gets the count of total eth

## Examples

### CLI

`a8 invoke crypto_eth.get_supply`

## Input

none or `{}`

## Output

count is number of eth mined up to this point

``` json
 {
   count: int
 }
```