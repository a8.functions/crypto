const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

const weiInEither = 1000000000000000000

fdk.handle(function(input){

   return api.stats.ethsupply().then((ethSupplyResult)=>{
    if(ethSupplyResult.status !== '1'){
     return {
       error : "Etherscan API failed"
     }
    }

    return {
      count : ethSupplyResult.result /weiInEither
    }
  })
})

fdk.slack(function(result){
  let responseType = "in_channel"
  let blocks = []

  if(result.error){
    responseType = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${result.error}`
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `Total ETH mined: ${numberWithCommas(result.count)}`
      }
    })
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
})

const numberWithCommas = (x) => {
  const numSplit = x.toString().split('.')
  numSplit[0] =  numSplit[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")

  return numSplit.join('.')
}



