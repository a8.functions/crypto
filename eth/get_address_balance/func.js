const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const api=require('etherscan-api').init('notarealkey')

fdk.handle(function(input){
  let address

  if( input._inputs ){
    address = input._inputs[0]
  }

  if (input.address) {
    address = input.address
  }

  if (!address){
      return {error: 'Please include a wallet address'}
  }

  const balance = api.account.balance(address)

  return balance.then((balanceData)=>{
     if(balanceData.status == 1){
       return { 
         eth: balanceData.result/1000000000000000000}
     }
  })
})

fdk.slack(function(result){
  let blocks = []
  let responseType = "in_channel"

  if(result.error){
    // we don't want to send the error condition to the channel
    responseType = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${result.error}`
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${result.eth} ETH`
      }
    })
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
})


// If input is passed in to slack as strings it is packaged and accessible via '_inputs: [var1, var2, ...]'
function getAddressFromInputs(input){
  let address

  if( input._inputs ){
    address = input._inputs[0]
  }

  if (input.address) {
    address = input.address
  }

  return address
}
