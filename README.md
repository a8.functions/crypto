# Namespace: crypto

## crypto.eth

### [get_usd_price](eth/get_usd_price)

| | |
|-|-|
| CLI   |  `a8 invoke crypto_eth.get_usd_price` |
| Slack   |  `/a8 crypto_eth.get_usd_price` |
| Discord |  `a8! crypto_eth.get_usd_price` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_eth.get_usd_price -d '{}'` |


### [get_gas_price](eth/get_gas_price)

| | |
|-|-|
| CLI     | `a8 invoke crypto_eth.get_gas_price` |
| Slack   | `/a8 crypto_eth.get_gas_price` |
| Discord | `a8! crypto_eth.get_gas_price` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_eth.get_gas_price -d '{}'` |

### [get_address_balance](eth/get_address_balance)

| | |
|-|-|
| CLI     | `a8 invoke crypto_eth.get_address_balance '{"address": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'` |
| Slack   | `/a8 crypto_eth.get_address_balance 0xB8c77482e45F1F44dE1745F52C74426C631bDD52` |
| Discord | `a8! crypto_eth.get_address_balance 0xB8c77482e45F1F44dE1745F52C74426C631bDD52` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_eth.get_address_balance -d '{"address": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'` |

### [get_supply](eth/get_supply)

| | |
|-|-|
| CLI     | `a8 invoke crypto_eth.get_supply` |
| Slack   | `/a8 crypto_eth.get_supply` |
| Discord | `a8! crypto_eth.get_supply` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_eth.get_supply -d '{}'` |

### [get_token_supply](eth/get_token_supply)

| | |
|-|-|
| CLI     | `a8 invoke crypto_eth.get_token_supply '{"address": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'` |
| Slack   | `/a8 crypto_eth.get_token_supply 0xB8c77482e45F1F44dE1745F52C74426C631bDD52` |
| Discord | `a8! crypto_eth.get_token_supply 0xB8c77482e45F1F44dE1745F52C74426C631bDD52` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_eth.get_token_supply -d '{"address": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'` |

### [get_address_first_use](eth/get_address_first_use)

| | |
|-|-|
| CLI     | `a8 invoke crypto_eth.get_address_first_use '{"address":"0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'` |
| Slack   | `/a8 crypto_eth.get_address_first_use 0xB8c77482e45F1F44dE1745F52C74426C631bDD52` |
| Discord | `a8! crypto_eth.get_address_first_use 0xB8c77482e45F1F44dE1745F52C74426C631bDD52` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_eth.get_address_first_use -d '{"address": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'` |

### [get_address_transactions](eth/get_address_transactions)

| | |
|-|-|
| CLI     | `a8 invoke crypto_eth.get_address_transactions '{"address": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_eth.get_address_transactions -d '{"address": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52"}'` |


## crypto.btc

### [get_usd_price](btc/get_usd_price)

| | |
|-|-|
| CLI   |  `a8 invoke crypto_btc.get_usd_price` |
| Slack   |  `/a8 crypto_btc.get_usd_price` |
| Discord |  `a8! crypto_btc.get_usd_price` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_btc.get_usd_price -d '{}'` |

### [get_supply](btc/get_supply)

| | |
|-|-|
| CLI   |  `a8 invoke crypto_btc.get_supply` |
| Slack   |  `/a8 crypto_btc.get_supply` |
| Discord |  `a8! crypto_btc.get_supply` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_btc.get_supply -d '{}'` |

### [get_market_cap_usd](btc/get_market_cap_usd)

| | |
|-|-|
| CLI   |  `a8 invoke crypto_btc.get_market_cap_usd` |
| Slack   |  `/a8 crypto_btc.get_market_cap_usd` |
| Discord |  `a8! crypto_btc.get_market_cap_usd` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_btc.get_market_cap_usd -d '{}'` |

### [get_address_balance](btc/get_address_balance)

| | |
|-|-|
| CLI   |  `a8 invoke crypto_btc.get_address_balance '{"address": "3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'` |
| Slack   |  `/a8 crypto_btc.get_address_balance 3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r` |
| Discord |  `a8! crypto_btc.get_address_balance 3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_btc.get_address_balance -d '{"address": "3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'` |

### [get_address_create_date](btc/get_address_create_date)

| | |
|-|-|
| CLI   |  `a8 invoke crypto_btc.get_address_create_date '{"address": "3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'` |
| Slack   |  `/a8 crypto_btc.get_address_create_date 3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r` |
| Discord |  `a8! crypto_btc.get_address_create_date 3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_btc.get_address_create_date -d '{"address": "3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'` |

### [get_transaction_result](btc/get_transaction_result)

| | |
|-|-|
| CLI   |  `a8 invoke crypto_btc.get_transaction_result '{"transaction": "d14fbf1b36222c7e7111a877b2f7e948ec65825da88da7a1683cbdf55990d369"}'` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto_btc.get_transaction_result -d '{"transaction": "d14fbf1b36222c7e7111a877b2f7e948ec65825da88da7a1683cbdf55990d369"}'` |

## crypto.charts

### [price_graph](charts/price_graph)

| | |
|-|-|
| CLI   |  `a8 invoke crypto.price_graph '{"currency":"BTC/USD"}'` |
| Slack   |  `/a8 crypto.price_graph currency:BTC/USD` |
| Discord |  `a8! crypto.price_graph currency:BTC/USD` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto.price_graph -d '{"currency":"BTC/USD"}'` |

### [price_chart](charts/price_chart)

| | |
|-|-|
| CLI   |  `a8 invoke crypto.price_chart  '{"currency":"BTC/USD", "exchange":"bitfinex"}'` |
| Slack   |  `/a8 crypto.price_chart  currency:BTC/USD, exchange:bitfinex` |
| Discord |  `a8! crypto.price_chart  currency:BTC/USD, exchange:bitfinex` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto.price_chart -d '{"currency":"BTC/USD","exchange":"bitfinex"}'` |

### [arbitrage_pairs](charts/arbitrage_pairs)

| | |
|-|-|
| CLI   |  `a8 invoke crypto.arbitrage_pairs '{"exchanges":["bittrex","bitfinex"],"currency":"BTC/USD"}'` |
| curl  |  `curl -XPOST https://web.a8.dev/invoke/crypto.arbitrage_pairs -d '{"exchanges":["bittrex","bitfinex"],"currency":"BTC/USD"}'` |

### [price_tickers](charts/price_tickers)

| | |
|-|-|
| CLI   |  `a8 invoke crypto.price_tickers '{"currency":"BTC/USD","exchange":"bitfinex"}'` |
| Slack   |  `/a8 crypto.price_tickers  currency:BTC/USD, exchange:bitfinex` |
| Discord |  `a8! crypto.price_tickers  currency:BTC/USD, exchange:bitfinex` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto.price_tickers -d '{"currency":"BTC/USD","exchange":"bitfinex"}'` |

### [trading_pairs](charts/trading_pairs)

| | |
|-|-|
| CLI   |  `a8 invoke crypto.trading_pairs '{"pairs":[ {"currency":"BTC/USD","exchange":"bittrex"} , {"currency":"BTC/USD","exchange":"bitfinex" }]}'` |

### [order_book](charts/order_book)

| | |
|-|-|
| CLI   |  `a8 invoke ccxt.order_book '{"currency":"BTC/USD","exchange":"bitfinex"}'` |
| Slack   |  `/a8 crypto.order_book  currency:BTC/USD, exchange:bitfinex` |
| Discord |  `a8! crypto.order_book  currency:BTC/USD, exchange:bitfinex` |
| curl    |  `curl -XPOST https://web.a8.dev/invoke/crypto.order_book -d '{"currency":"BTC/USD","exchange":"bitfinex"}'` |
