const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){
  return blockchain.statistics.get({}).then((result)=>{
    return {
      usd: result.market_price_usd * result.totalbc / 100000000
    }
  })
})

fdk.slack(function(result){
  let blocks = []
  let response_type = "in_channel"
    
  if (!result.usd) {
    response_type = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: "Something went wrong"
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `$${numberWithCommas(result.usd)}`
      }
    })
  }

  return {
    "response_type": response_type,
    "blocks": blocks
  }
})

const numberWithCommas = (x) => {
  const numSplit = x.toString().split('.')
  numSplit[0] =  numSplit[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")

  return numSplit.join('.')
}
