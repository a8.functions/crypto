# get_market_cap_usd

gets market cap of bitcoin in usd

## Examples

### CLI

`a8 invoke crypto_btc.get_market_cap_usd`

## Input

`{}` or none

## Output

`USD` market cap of btc in usd  

``` json
 {
    USD: int
 }
```