const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){
  if(!input.transaction){
    return {
      error : 'no address entered'
    }
  }

  const transaction = input.transaction

  return blockchain.blockexplorer.getTx(transaction)
})
