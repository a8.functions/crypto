# get_transaction_result

#### example calls:
##### cli
`a8 invoke crypto_btc.get_transaction_result '{"transaction":"d14fbf1b36222c7e7111a877b2f7e948ec65825da88da7a1683cbdf55990d369"}'`

##### slack
`/a8 invoke crypto_btc.get_transaction_result(transaction:d14fbf1b36222c7e7111a877b2f7e948ec65825da88da7a1683cbdf55990d369)`


This function let one look at a blockchain transaction inputs and outpus
## input
`{ transaction: [transactionHash] }`

## output
 ```{ ver: 2,
    inputs:
     [ { sequence: ,
         witness: '',
         prev_out: ,
         script:
     } ],
    weight: ,
    block_height: ,
    relayed_by: ,
    out:
     [ { spent: ,
         spending_outpoints: ,
         tx_index: ,
         type: ,
         addr: ,
         value: ,
         n: ,
         script:  },
       ],
    lock_time: ,
    size: ,
    double_spend: ,
    block_index: ,
    time: ,
    tx_index: ,
    vin_sz: ,
    hash, ,
    vout_sz:  }
    
  ```