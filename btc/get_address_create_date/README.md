# get_address_create_date

Gets the date of the first transaction an address made

## Examples

### CLI

`a8 invoke crypto_btc.get_address_create_date  '{"address":"3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'`

## Input

`address` is a btc address

``` json
{
    address : string
}
```

## Output

`unixTime` is the time in seconds since unix epoch.
`niceTime` is a well formated human readable string , eg `'2 Nov 2018 11:27:44`

``` json
 {
    unixTime: int,
    niceTime: DD Mon YYYY hh:mm:ss
 }
```