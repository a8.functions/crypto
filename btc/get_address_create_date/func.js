const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){
  const address = getAddressFromInputs(input)
  if(!address){
    return {
      error : 'Please provide a wallet address'
    }
  }

  return blockchain.blockexplorer.getAddress(address)
    .then((result) => {
      return result.txs.reduce((acc, tx) => {
        return tx.time < acc ? tx.time : acc
      }, Infinity)
    })
    .then((unixTime) => {
      return {
        unixTime: unixTime,
        niceTime: timeConverter(unixTime)
      }
    })

})

fdk.slack(function(result){
  let blocks = []
  let response_type = "in_channel"
    
  if(result.error) {
    response_type = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${result.error}`
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `<!date^${result.unixTime}^First Transaction {date_num} {time_secs}| ${result.niceTime}>`
      }
    })
  }

  return {
    "response_type": response_type,
    "blocks": blocks
  }
})


const timeConverter = (UNIX_timestamp) => {
  const a = new Date(UNIX_timestamp * 1000)
  const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
  const year = a.getFullYear()
  const month = months[a.getMonth()]
  const date = a.getDate()
  const hour = a.getHours()
  const min = a.getMinutes()
  const sec = a.getSeconds()
  return date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec
}

// If input is passed in to slack as strings it is packaged and accessible via '_inputs: [var1, var2, ...]'
function getAddressFromInputs(input){
  let address

  if( input._inputs ){
    address = input._inputs[0]
  }

  if (input.address) {
    address = input.address
  }

  return address
}
