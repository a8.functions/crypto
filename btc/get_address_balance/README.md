# get_address_balance

Gets btc balance of an address

## Examples

### CLI

`a8 invoke crypto_btc.get_address_balance  '{"address":"3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r"}'`

## Input

`address` is a BTC address

``` json
{
    address : string
}
```

## Output

`BTC` is the amount of btc currently associated with the address  

``` json
 {
    BTC: int
 }
```