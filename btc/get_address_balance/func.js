const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){

  const address = getAddressFromInputs(input)

  if (!address) {
    return {
      error : 'Please provide a wallet address'
    }
  }

  return blockchain.blockexplorer.getBalance(address)
    .then((result) => {
      return {
        btc: result[address].final_balance/100000000
      }
    })
})

fdk.slack(function(result){
  let responseType = "in_channel"
  let blocks = []

  if (result.error) {
    responseType = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${result.error}`
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `${result.btc} BTC`
      }
    })
  }

  return {
    "response_type": responseType,
    "blocks": blocks
  }
})

// If input is passed in to slack as strings it is packaged and accessible via '_inputs: [var1, var2, ...]'
function getAddressFromInputs(input){
  let address

  if (input._inputs) {
    address = input._inputs[0]
  }

  if (input.address) {
    address = input.address
  }

  return address
}
