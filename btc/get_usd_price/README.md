# get_usd_price
Gets price of an amount of bitcoin in usd 

## Examples

### CLI

`a8 invoke crypto_btc.get_usd_price`

## Input

`BTC` is number of BTC, it defaults to 1

``` json
{
    BTC : int
}
```

## Output

`USD` is the amount in USD of the BTC

``` json
 {
    USD: int,  
 }
```
