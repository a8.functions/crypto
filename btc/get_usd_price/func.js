const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){
  const btc = getBtcFromInputs(input)

  return blockchain.statistics.get({stat:'market_price_usd'})
    .then((result)=>{
      return {
        USD : result * btc
      }
    })
    .catch((err) => {
      return {"error": "Something went wrong"}
    })
})

fdk.slack(function(result){
  let blocks = []
  let response_type = "in_channel"
    
  if(result.error){
    response_type = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: result.error
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text:  `$${result.USD}`
      }
    })
  }

  return {
    "response_type": response_type,
    "blocks": blocks
  }
})

fdk.sheets(function(result) {
  return {
    "sheets": [['BTC',`${result.USD}`]]
  }
})

// If input is passed in to slack as strings it is packaged and accessible via '_inputs: [var1, var2, ...]'
function getBtcFromInputs(input){
  let btc = 0

  if( input._inputs ){
    btc = input._inputs[0]
  }

  if (input.BTC) {
    btc = input.BTC
  }

  if (btc === 0){
    btc = 1
  }

  return btc
}
