const fdk=require('@autom8/fdk')
const a8=require('@autom8/js-a8-fdk')
const blockchain= require('blockchain.info')

fdk.handle(function(input){
  return blockchain.statistics.get(/*{stat:'totalbc'}*/)
    .then((result) => {
      return {
        count: result.totalbc / 100000000
      }
    })

 })

fdk.slack(function(result){
  let blocks = []
  let response_type = "in_channel"
    
  if(result.error){
    response_type = "ephemeral"
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: result.error
      }
    })
  } else {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `Total BTC mined: ${numberWithCommas(result.count)}`
      }
    })
  }

  return {
    "response_type": response_type,
    "blocks": blocks
  }
})

const numberWithCommas = (x) => {
  const numSplit = x.toString().split('.')
  return numSplit[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
