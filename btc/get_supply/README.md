# get_supply

Gets the count of total btc

## Examples

### CLI

`a8 invoke crypto_btc.get_supply`

## Input

none or `{}`

## Output

count is number of btc mined up to this point

``` json
 {
   count: int
 }
```