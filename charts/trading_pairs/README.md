# trading_pairs

Gets trading pairs chart for provided currencies/markets

## Examples

### CLI

`a8 invoke crypto.trading_pairs`

#### input

`currency` is currency (required)

`exchange` is exchange market (required),
list of supported exchanges can be found [here](https://github.com/ccxt/ccxt#supported-cryptocurrency-exchange-markets)

`interval` can be minutes/hours/days for example `15m` or `1h`, default `1h`

`theme` can be `ligth` or `dark` default `light`
 

``` json
{
     pairs: [ 
        { 
            exchange: string,
            market: string
        },
        { 
            exchange: string,
            market: string
        }
     ],
     from: string,
     to: string,
     theme: string
}
```

#### Output

`data` with chart image url

``` json
 {
    url: string  
 }
```