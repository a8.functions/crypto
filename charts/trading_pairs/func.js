const fdk = require('@autom8/fdk')
const tradingPairs = require('./trading-pairs')

const handle = (input) => {

  const params = {
    pairs: input.pairs,
    from: input.from,
    to: input.to,
    theme: input.theme,
    _transformer: input._transformer
  }
  if (!params.pairs)
    return {error: 'Please provide trading pairs'}

  return tradingPairs.chart(params)
    .then(response => {
      return {data: response}
    })
    .catch(err => {
      return {error: err.message}
    })
}

const slack = (result) => {
  let responseType = 'in_channel'
  const blocks = []

  if (!result.error) {
    blocks.push({
      'type': 'image',
      'title': {
        'type': 'plain_text',
        'text': 'Trading pairs chart',
        'emoji': true
      },
      'image_url': result.url,
      'alt_text': 'chart'
    })
  } else {
    responseType = 'ephemeral'
    blocks.push({
      'type': 'section',
      'text': {
        'type': 'mrkdwn',
        'text': 'Error generating chart. ' + result.error
      }
    })
  }

  return {
    'response_type': responseType,
    'blocks': blocks
  }
}

const discord = (result) => {

  if (!result.error) {
    return {
      'embed': {
        'image': {
          'url': result.url
        }
      }
    }
  } else {
    return {
      'content': 'Error generating chart.  ' + result.error
    }
  }
}
// module.exports = { handle }
fdk.handle(handle)
fdk.slack(slack)
fdk.discord(discord)
