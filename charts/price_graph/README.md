# price_graph

Gets price graph

## Examples

### CLI

`a8 invoke crypto.price_graph`

#### input

`currency` is currency (required)

`exchange` is exchange market (required),
list of supported exchanges can be found [here](https://github.com/ccxt/ccxt#supported-cryptocurrency-exchange-markets)

`interval` can be minutes/hours/days for example `15m` or `1h`, default `1h` 

`from/to` can be the time in seconds since unix epoch or a well formatted human readable string , eg `2 Nov 2018 11:27:44`
 

``` json
{
    currency: string,
    exchange:  string,
    interval: string,
    from: string,
    to: string
}
```

#### Output

`data` with chart image url

``` json
 {
    url: string  
 }
```