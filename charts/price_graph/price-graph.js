const a8 = require('@autom8/js-a8-fdk')

const graph = async (params) => {

    const priceData = await a8.ccxt.ohlcv(params)

    if (priceData.error) {
        throw new Error("error data generating")
    }

    const chart = await a8.graphs.ascii_chart({ ohlcv: priceData.ohlcv })
    if (chart.error) {
        throw new Error(chart.error)
    }

    return chart.url
}

module.exports = { graph }