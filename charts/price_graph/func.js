const fdk = require('@autom8/fdk')
const priceGraph = require('./price-graph')

const handle = (input) => {

  return priceGraph.graph(input)
    .then(response => {
      return {url: response}
    })
    .catch(err => {
      return {error: err}
    })
}

const slack = (result) => {
  let responseType = 'in_channel'
  const blocks = []

  if (!result.error) {
    blocks.push({
      'type': 'image',
      'title': {
        'type': 'plain_text',
        'text': 'Price graph',
        'emoji': true
      },
      'image_url': result.url,
      'alt_text': 'chart'
    })
  } else {
    responseType = 'ephemeral'
    blocks.push({
      'type': 'section',
      'text': {
        'type': 'mrkdwn',
        'text': 'Error generating chart. ' + result.error
      }
    })
  }

  return {
    'response_type': responseType,
    'blocks': blocks
  }
}

const discord = (result) => {

  if (!result.error) {
    return {
      'embed': {
        'image': {
          'url': result.url
        }
      }
    }
  } else {
    return {
      'content': 'Error generating chart.  ' + result.error
    }
  }
}

fdk.handle(handle)
fdk.slack(slack)
fdk.discord(discord)
