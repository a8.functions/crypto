const fdk = require('@autom8/fdk')
const orderBook = require('./order-book')

const handle = (input) => {

  const params = {
    currency: input.currency,
    exchange: input.exchange
  }

  if (!params.exchange)
    return {error: 'Please provide exchange'}
  else if (!params.currency)
    return {error: 'Please provide exchanges'}
  else
    return orderBook.chart(params)
      .then(response => {
        return {url: response}
      })
      .catch(err => {
        return {error: err.message}
      })
}


const slack = (result) => {
  let responseType = 'in_channel'
  const blocks = []

  if (!result.error) {
    blocks.push({
      'type': 'image',
      'title': {
        'type': 'plain_text',
        'text': 'Order book chart',
        'emoji': true
      },
      'image_url': result.url,
      'alt_text': 'chart'
    })
  } else {
    responseType = 'ephemeral'
    blocks.push({
      'type': 'section',
      'text': {
        'type': 'mrkdwn',
        'text': 'Error generating chart. ' + result.error
      }
    })
  }

  return {
    'response_type': responseType,
    'blocks': blocks
  }
}

const discord = (result) => {

  if (!result.error) {
    return {
      'embed': {
        'image': {
          'url': result.url
        }
      }
    }
  } else {
    return {
      'content': 'Error generating chart.  ' + result.error
    }
  }
}

fdk.handle(handle)
fdk.slack(slack)
fdk.discord(discord)
