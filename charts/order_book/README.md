# order_book

Gets order book chart 

## Examples

### CLI

`a8 invoke crypto.order_book`

#### input

`currency` is currency (required)

`exchange` is exchange market (required),
list of supported exchanges can be found [here](https://github.com/ccxt/ccxt#supported-cryptocurrency-exchange-markets)


``` json
{
    currency: string,
    exchange: string,    
}
```

#### Output

`data` with chart image url

``` json
 {
    url: string  
 }
```