const a8 = require('@autom8/js-a8-fdk')

const chart = async (params) => {

  const data = await a8.ccxt.order_book(params)
  if (data.error) {
    throw new Error(data.error)
  }

  const chart = await a8.graphs.order_book_chart(data)

  if (chart.error) {
    throw new Error(chart.error)
  }

  return chart.url
}

module.exports = {chart}