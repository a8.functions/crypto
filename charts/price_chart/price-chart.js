const a8 = require('@autom8/js-a8-fdk')

const graph = async (params) => {

  const priceData = await a8.ccxt.ohlcv(params)

  if (priceData.error) {
    throw new Error(priceData.error)
  }
  let colors

  if (params.theme == 'dark')
    colors = {
      background: '#292f3b',
      xAxis: 'darkgrey',
      candleRaise: 'lightgrey',
      candleFall: 'turquoise',
      volumeBar: 'darkgrey'
    }
  else
    colors = {
      background: '#ffffff',
      xAxis: '#292f3b',
      candleRaise: '#64b5f5',
      candleFall: '#ef6c00',
      volumeBar: '#8cc6f5'
    }

  const chart = await a8.graphs.candlestick_chart({ohlcv: priceData.ohlcv, colors, _transformer: params._transformer})

  if (chart.error) {
    throw new Error(chart.error)
  }

  return chart.data
}

module.exports = {graph}