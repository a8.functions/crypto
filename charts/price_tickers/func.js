const fdk = require('@autom8/fdk')
const priceTickers = require('./price-tickers')

const handle = (input) => {

  const params = {
    currency: input.currency,
    exchange: input.exchange,
    theme: input.theme,
    _transformer: input._transformer
  }
  if (!params.exchange || !params.currency)
    return {error: 'Please provide exchange'}
  if (!params.currency)
    return {error: 'Please provide currency'}

  return priceTickers.chart(params)
    .then(response => {
      return {data: response}
    })
    .catch(err => {
      return {error: err.message}
    })
}

const slack = (result) => {
  let responseType = 'in_channel'
  const blocks = []

  if (!result.error) {
    blocks.push({
      'type': 'image',
      'title': {
        'type': 'plain_text',
        'text': 'Price tickers chart',
        'emoji': true
      },
      'image_url': result.url,
      'alt_text': 'chart'
    })
  } else {
    responseType = 'ephemeral'
    blocks.push({
      'type': 'section',
      'text': {
        'type': 'mrkdwn',
        'text': 'Error generating chart. ' + result.error
      }
    })
  }

  return {
    'response_type': responseType,
    'blocks': blocks
  }
}

const discord = (result) => {

  if (!result.error) {
    return {
      'embed': {
        'image': {
          'url': result.url
        }
      }
    }
  } else {
    return {
      'content': 'Error generating chart.  ' + result.error
    }
  }
}

fdk.handle(handle)
fdk.slack(slack)
fdk.discord(discord)
