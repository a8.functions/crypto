# price_tickers

Gets price tickers chart for last 24 hours

## Examples

### CLI

`a8 invoke crypto.price_tickers`

#### input

`currency` is currency (required)

`exchange` is exchange market (required),
list of supported exchanges can be found [here](https://github.com/ccxt/ccxt#supported-cryptocurrency-exchange-markets)

`interval` can be minutes/hours/days for example `15m` or `1h`, default `1h`,

`theme` can be `ligth` or `dark` default `light` 
 

``` json
{
    currency: string,
    exchange: string,
    interval: string,
    theme: string
}
```

#### Output

`data` with chart image url

``` json
 {
    url: string  
 }
```