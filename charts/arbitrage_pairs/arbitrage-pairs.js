const a8 = require('@autom8/js-a8-fdk')

const chart = async (params) => {

  const ohlcv = []
  await Promise.all(
    params.exchanges.map(async exchange => {
      const data = await a8.ccxt.ohlcv({exchange, ...params})
      if (data.error) {
        throw new Error(data.error)
      }
      data.ohlcv.map(e => {
        e.symbol = exchange
        ohlcv.push({...e, symbol: exchange})
      })
    })
  )

  let colors

  if (params.theme == 'dark')
    colors = {
      background: '#292f3b',
      xAxis: 'darkgrey',
      color1: 'orange',
      color2: 'steelblue'
    }
  else
    colors = {
      background: '#ffffff',
      xAxis: 'grey',
      color1: 'orange',
      color2: 'steelblue'
    }
  const chart = await a8.graphs.multiline_chart({ohlcv, colors, _transformer: params._transformer})

  if (chart.error) {
    throw new Error(chart.error)
  }

  return chart.data
}

module.exports = {chart}