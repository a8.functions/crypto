# arbitrage_pairs

Gets arbitrage pairs chart 

## Examples

### CLI

`a8 invoke crypto.arbitrage_pairs`

#### input

`currency` is currency to compare (required)

`exchanges` array of 2 exchange markets (required),
list of supported exchanges can be found [here](https://github.com/ccxt/ccxt#supported-cryptocurrency-exchange-markets)

`interval` can be minutes/hours/days for example `15m` or `1h`, default `1h` 

`from/to` can be the time in seconds since unix epoch or a well formatted human readable string , eg `2 Nov 2018 11:27:44`

`theme` can be `ligth` or `dark` default `dark` 

``` json
{
    currency: string,
    exchanges: [ string ],
    interval: string,
    from: string,
    to: string,
    theme: string
}
```

#### Output

`data` with chart image url

``` json
 {
    url: string  
 }
```